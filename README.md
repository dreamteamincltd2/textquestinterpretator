Проект TextQuestInterpreter

Эксперты: Бычик, Дурнев, Пичкур.

Программисты: Ивин, Пичугин.

Тестировщики: Михальченко, Казинцев. 

Руководство по сборке и запуску

Скачать данный репозиторий на компьютер, на котором планируется тестирование
(перед установкой у вас должен быть установлен android sdk, и переменная #ANDROID_HOME в путях указывала на него, либо прописать путь до android sdk в файле проекта local.properties)
(плюс у вас должны быть установлены библиотеки из android sdk, (android 4.4.2 api 19) из sdk manager)
В скачанной папке будет лежать скрипт автоматической сборки gradlew.bat (gradlew для linux)
Зайти в консоль и в ней перейти в каталог, в котором лежит gradlew.bat файл
Для сборки проекта и создания исполняемого .apk файла необходимо ввести следующую команду:
Для debug сборки и запуска на первом запущенном эмуляторе или подключенном телефоне
> gradlew android::installDebug android::run

*Для сборки обычного исполняемого .apk файла
> gradlew android::assembleDebug
(исполняемые файлы будут лежать в папке проекта по пути app/build/outputs/apk)