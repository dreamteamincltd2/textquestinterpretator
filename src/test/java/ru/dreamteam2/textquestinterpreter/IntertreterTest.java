package ru.dreamteam2.textquestinterpreter;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.hasItems;

import org.junit.*;
import org.junit.rules.ExpectedException;
import ru.dreamteam2.textquestinterpreter.questengine.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * Created by arssivka on 11/13/14.
 */
public class IntertreterTest {
    public static final String TEST1_QF = "./src/test/resources/ru/dreamteam2/textquestinterpreter/test1.quest";
    public static final String TEST2_QF = "./src/test/resources/ru/dreamteam2/textquestinterpreter/test2.quest";

    @Test
    public void interpreterTest1() throws FileNotFoundException, ParseException {
        QuestLanguageParser parser = new QuestLanguageParser(new FileReader(TEST1_QF));
        QuestInterpreter interpreter = new QuestInterpreter(parser.program());
        interpreter.toScene("Quest");
    }

    @Test
    public void interpreterTest2() throws FileNotFoundException, ParseException {
        QuestLanguageParser parser = new QuestLanguageParser(new FileReader(TEST2_QF));
        QuestInterpreter interpreter = new QuestInterpreter(parser.program());
        interpreter.toScene("Fibonacci");
        Assert.assertEquals("The 12 number fibonachi is 144", interpreter.getText(), "144");
    }
}
