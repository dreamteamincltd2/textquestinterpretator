package ru.dreamteam2.textquestinterpreter.questengine;

import java.awt.*;

/**
 * Created by arssivka on 11/4/14.
 */
public class QuestAction {
    private Image mIcon;
    private String mText;
    private String mTargetScene;

    public QuestAction(String text, String targetScene) {
        this.mText = text;
        this.mTargetScene = targetScene;
    }

    public QuestAction(Image icon, String text, String targetScene) {
        this.mIcon = icon;
        this.mText = text;
        this.mTargetScene = targetScene;
    }

    @Override
    public String toString() {
        return mText;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QuestAction that = (QuestAction) o;

        if (mIcon != null ? !mIcon.equals(that.mIcon) : that.mIcon != null) return false;
        if (!mTargetScene.equals(that.mTargetScene)) return false;
        if (!mText.equals(that.mText)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = mIcon != null ? mIcon.hashCode() : 0;
        result = 31 * result + mText.hashCode();
        result = 31 * result + mTargetScene.hashCode();
        return result;
    }

    public Image getIcon() {
        return mIcon;
    }

    public void setIcon(Image icon) {
        this.mIcon = icon;
    }

    public String getText() {
        return mText;
    }

    public String getTargetScene() {
        return mTargetScene;
    }
}
