package ru.dreamteam2.textquestinterpreter.questengine;

import javax.swing.*;
import java.util.Hashtable;
import java.util.LinkedList;

/**
 * Created by arssivka on 11/4/14.
 */
public class QuestInterpreter {
    private QuestNodeInterpreter mNodeInterpreter;
    private SimpleNode mProgram;

    public QuestInterpreter(SimpleNode program) {
        setProgram(program);
    }

    public QuestInterpreter(String source) throws ParseException {
        setProgram(source);
    }

    /**
     * @return Возвращает текст, содержащийся в текущей сцене
     */
    public String getText() {
        if (mNodeInterpreter != null) {
            return mNodeInterpreter.getText();
        } else {
            return null;
        }
    }

    /**
     * Совершает переход к сцене, заданной в QuestAction
     *
     * @param action
     * @throws RuntimeException
     */
    public void toScene(QuestAction action) throws RuntimeException {
        this.toScene(action.getTargetScene());
    }

    /**
     * Совершает переход к сцене
     *
     * @param scene
     * @throws RuntimeException
     */
    public void toScene(String scene) throws RuntimeException {
        if (mNodeInterpreter == null || mProgram == null) {
            throw new RuntimeException("Quest program isn't set.");
        }

        mNodeInterpreter.visit(mProgram, scene);
    }

    /**
     * Задает исходный код и преобразует в интерпретируемый вид квестовую программу
     *
     * @param source
     * @return
     * @throws ParseException
     */
    public boolean setProgram(String source) throws ParseException {
        QuestLanguageParser parser = new QuestLanguageParser(new java.io.StringReader(source));
        return this.setProgram(parser.program());
    }

    /**
     * Устанавливает квестовую программу. Node должен иметь тип Program
     *
     * @param program
     * @return
     */
    public boolean setProgram(SimpleNode program) {
        if (program.toString().equals("Program")) {
            mNodeInterpreter = new QuestNodeInterpreter();
            mProgram = program;
            reset();
            return true;
        }
        return false;
    }

    /**
     * Устанавливает начальные значения интерпретатора
     */
    public void reset() {
        mNodeInterpreter.reset();
    }

    /**
     * Служебный класс интерпретатора, основанный на реализации интерфейса QuestLanguageParserVisitor
     */
    protected class QuestNodeInterpreter implements QuestLanguageParserVisitor {
        private final LinkedList<Hashtable<String, Object>> mMemory = new LinkedList<Hashtable<String, Object>>();
        private final Object[] mStack = new Object[1024];
        private StringBuilder mText;

        public QuestNodeInterpreter() {
            this.reset();
        }

        public String getText() {
            return mText.toString();
        }


        @Override
        public Object visit(SimpleNode node, Object data) throws RuntimeException {
            String nodeName = node.toString();

            if (nodeName.equals("Program")) {
                // В data должна быть передано название сцены или функции
                if (data == null) {
                    throw new RuntimeException("Scene or function name isn't set.");
                }

                String id = (String) data;
                boolean found = false;
                for (int i = 0; i < node.jjtGetNumChildren(); ++i) {
                    SimpleNode child = (SimpleNode) node.jjtGetChild(i);
                    if (child != null && id.equals(child.jjtGetValue())) {
                        found = true;
                        String childName = child.toString();
                        if (childName.equals("Scene")) {
                            this.visit(child, null);
                        } else {
                            throw new RuntimeException("Unsupported program unit.");
                        }
                        break;
                    }
                }

                if (!found) {
                    throw new RuntimeException("Program unit isn't found.");
                }
            } else if (nodeName.equals("Scene")) {
                this.visit((SimpleNode) node.jjtGetChild(0), false);
            } else if (nodeName.equals("Block")) {
                boolean ownScope = data instanceof Boolean && (Boolean) data != false;
                if (ownScope) {
                    mMemory.addFirst(new Hashtable<String, Object>());
                }

                for (int i = 0; i < node.jjtGetNumChildren(); ++i) {
                    this.visit((SimpleNode) node.jjtGetChild(i), null);
                }

                if (ownScope) {
                    mMemory.removeFirst();
                }
            } else if (nodeName.equals("ClearStatement")) {
                clear();
            } else if (nodeName.equals("StatementExpression")) {
                this.visit((SimpleNode) node.jjtGetChild(0), null);
            } else if (nodeName.equals("Assignment")) {
                return setValue((String) ((SimpleNode) node.jjtGetChild(0)).jjtGetValue(),
                        this.visit((SimpleNode) node.jjtGetChild(1), null));
            } else if (nodeName.equals("AddNode")) {
                Object val1 = this.visit((SimpleNode) node.jjtGetChild(0), null);
                Object val2 = this.visit((SimpleNode) node.jjtGetChild(1), null);

                if (val1 instanceof Integer && val2 instanceof Integer) {
                    return (Integer) val1 + (Integer) val2;
                } else if (val1 instanceof String && val2 instanceof String) {
                    return (String) val1 + (String) val2;
                }

                throw new RuntimeException("Unsupported " + val1.getClass() + " and " + val2.getClass() + " in add operation");
            } else if (nodeName.equals("SubtractNode")) {
                Object val1 = this.visit((SimpleNode) node.jjtGetChild(0), null);
                Object val2 = this.visit((SimpleNode) node.jjtGetChild(1), null);

                if (val1 instanceof Integer && val2 instanceof Integer) {
                    return (Integer) val1 - (Integer) val2;
                }
                throw new RuntimeException("Unsupported " + val1.getClass() + " and " + val2.getClass() + " in subtract operation");
            } else if (nodeName.equals("MulNode")) {
                Object val1 = this.visit((SimpleNode) node.jjtGetChild(0), null);
                Object val2 = this.visit((SimpleNode) node.jjtGetChild(1), null);

                if (val1 instanceof Integer && val2 instanceof Integer) {
                    return (Integer) val1 * (Integer) val2;
                }
                throw new RuntimeException("Unsupported " + val1.getClass() + " and " + val2.getClass() + " in mul operation");
            } else if (nodeName.equals("DivNode")) {
                Object val1 = this.visit((SimpleNode) node.jjtGetChild(0), null);
                Object val2 = this.visit((SimpleNode) node.jjtGetChild(1), null);

                if (val1 instanceof Integer && val2 instanceof Integer) {
                    return (Integer) val1 / (Integer) val2;
                }
                throw new RuntimeException("Unsupported " + val1.getClass() + " and " + val2.getClass() + " in div operation");
            } else if (nodeName.equals("ModNode")) {
                Object val1 = this.visit((SimpleNode) node.jjtGetChild(0), null);
                Object val2 = this.visit((SimpleNode) node.jjtGetChild(1), null);

                if (val1 instanceof Integer && val2 instanceof Integer) {
                    return (Integer) val1 % (Integer) val2;
                }
                throw new RuntimeException("Unsupported " + val1.getClass() + " and " + val2.getClass() + " in mod operation");
            } else if (nodeName.equals("AndNode")) {
                return (Boolean) this.visit((SimpleNode) node.jjtGetChild(0), null)
                        && (Boolean) this.visit((SimpleNode) node.jjtGetChild(1), null);
            } else if (nodeName.equals("OrNode")) {
                return (Boolean) this.visit((SimpleNode) node.jjtGetChild(0), null)
                        || (Boolean) this.visit((SimpleNode) node.jjtGetChild(1), null);
            } else if (nodeName.equals("NotNode")) {
                Boolean expr = (Boolean) this.visit((SimpleNode) node.jjtGetChild(0), null);
                return !expr;
            } else if (nodeName.equals("EQNode")) {
                Object val1 = this.visit((SimpleNode) node.jjtGetChild(0), null);
                Object val2 = this.visit((SimpleNode) node.jjtGetChild(1), null);
                return val1.equals(val2);
            } else if (nodeName.equals("NENode")) {
                Object val1 = this.visit((SimpleNode) node.jjtGetChild(0), null);
                Object val2 = this.visit((SimpleNode) node.jjtGetChild(1), null);
                return !val1.equals(val2);
            } else if (nodeName.equals("LTNode")) {
                Integer val1 = (Integer) this.visit((SimpleNode) node.jjtGetChild(0), null);
                Integer val2 = (Integer) this.visit((SimpleNode) node.jjtGetChild(1), null);
                return val1 < val2;
            } else if (nodeName.equals("GTNode")) {
                Integer val1 = (Integer) this.visit((SimpleNode) node.jjtGetChild(0), null);
                Integer val2 = (Integer) this.visit((SimpleNode) node.jjtGetChild(1), null);
                return val1 > val2;
            } else if (nodeName.equals("LENode")) {
                Integer val1 = (Integer) this.visit((SimpleNode) node.jjtGetChild(0), null);
                Integer val2 = (Integer) this.visit((SimpleNode) node.jjtGetChild(1), null);
                return val1 <= val2;
            } else if (nodeName.equals("GENode")) {
                Integer val1 = (Integer) this.visit((SimpleNode) node.jjtGetChild(0), null);
                Integer val2 = (Integer) this.visit((SimpleNode) node.jjtGetChild(1), null);
                return val1 >= val2;
            } else if (nodeName.equals("Id")) {
                String var = (String) ((SimpleNode) node).jjtGetValue();
                return this.getValue(var);
            } else if (nodeName.equals("IntConstNode")) {
                return ((SimpleNode) node).jjtGetValue();
            } else if (nodeName.equals("StringConstNode")) {
                return ((SimpleNode) node).jjtGetValue();
            } else if (nodeName.equals("TrueNode")) {
                return true;
            } else if (nodeName.equals("FalseNode")) {
                return false;
            } else if (nodeName.equals("IfStatement")) {
                Boolean expr = (Boolean) this.visit((SimpleNode) node.jjtGetChild(0), null);
                if (expr) {
                    this.visit((SimpleNode) node.jjtGetChild(1), null);
                } else if (node.jjtGetNumChildren() >= 3) {
                    this.visit((SimpleNode) node.jjtGetChild(2), null);
                }
            } else if (nodeName.equals("PrintStatement")) {
                this.print(this.visit((SimpleNode) node.jjtGetChild(0), null));
            } else if (nodeName.equals("WhileStatement")) {
                while ((Boolean) this.visit((SimpleNode) node.jjtGetChild(0), null)) {
                    this.visit((SimpleNode) node.jjtGetChild(1), null);
                }
            } else {
                throw new UnsupportedOperationException("Unsupported node " + nodeName + ".");
            }
            return null;
        }

        /**
         * Печатает текст в сцену
         *
         * @param obj
         */
        public void print(Object obj) {
            mText.append(obj);
        }

        /**
         * Устанавливает интерпретатор в начальное состояние
         */
        public void reset() {
            this.clear();
            mMemory.clear();
            mMemory.push(new Hashtable<String, Object>());
        }

        /**
         * Очищает текущее содержимое сцены
         */
        private void clear() {
            mText = new StringBuilder();
        }

        /**
         * Возвращает значение переменной, хранящийся в памяти интерпретатора
         *
         * @param vName имя переменной
         * @return значение переменной
         */
        public Object getValue(String vName) {
            Object val = null;
            for (Hashtable<String, Object> mem : mMemory) {
                if ((val = mem.get(vName)) != null) {
                    break;
                }
            }

            return val;
        }

        /**
         * Устанавливает значение переменной, хранящийся в памяти интерпретатора.
         * значение null удаляет переменную из памяти
         *
         * @param vName имя переменной
         * @param value новое значение
         */
        public Object setValue(String vName, Object value) {
            for (Hashtable<String, Object> mem : mMemory) {
                if (mem.containsKey(vName)) {
                    if (value != null) {
                        mem.put(vName, value);
                    } else {
                        mem.remove(vName);
                    }
                    return value;
                }
            }

            if (value != null) {
                mMemory.getFirst().put(vName, value);
            }
            return value;
        }
    }
}
